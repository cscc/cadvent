Description
===

cadvent is a simple RESTful microservice for coding "adverse events" against
the MedDRA ^(TM) database.

It performs a phonetic search for user input, and provides a simple, 
easy-to-parse JSON output of the results.

Currently handles lowest-level terms (LLTs) and preferred terms (PTs); 
see MedDRA ^(TM) for definitions.

Usage
===

Build with maven:
```
    mvn package
```

Then run, specifying "base directory" (where cadvent will look for `llt.asc` and 
`pt.asc` from MedAscii from your MedDRA download):
```
    java -jar <path-to-cadvent.jar> --cadvent.base_directory=<your path>
```

Assuming all goes well, you will have a running service at 
http://your\_hostname\_here:8080/search

It takes a single GET parameter "query" with the search term.  Its response 
should be pretty self explanatory.

Usage - Docker
===

Using a combination of the GitLab CI infrastructure and the bundled Dockerfile,
we now automatically produce a Docker image capable of running cadvent.  Check
out [our container registry](https://gitlab.com/cscc/cadvent/container_registry)
if you want to use it.  (As you may notice, the Dockerfile requires the MedDRA
dataset to be present at `/data/`, so you'll need to mount that when you
run the image...)


License
===

BSD 3-clause. See `LICENSE.txt` and `THIRD_PARTY-LICENSE.txt` for details.

Questions?  Comments?  Threats?
===

Visit http://cscc.io/contact