DROP TABLE IF EXISTS "lowest_level_terms";
DROP INDEX IF EXISTS llt_pref_term_code_idx;
DROP INDEX IF EXISTS llt_hash_idx;
DROP INDEX IF EXISTS llt_hash_type_idx;
DROP INDEX IF EXISTS llt_term_idx;

DROP TABLE IF EXISTS "preferred_terms";
DROP INDEX IF EXISTS pt_hash_idx;
DROP INDEX IF EXISTS pt_hash_type_idx;
DROP INDEX IF EXISTS pt_term_idx;


CREATE TABLE "preferred_terms" (
	"code"					INTEGER PRIMARY KEY NOT NULL,
	"term"					VARCHAR(1024) NOT NULL,
	
	"phonetic_hash"			VARCHAR(64) NOT NULL,
	"phonetic_hash_type"	VARCHAR(128) NOT NULL
);

CREATE INDEX IF NOT EXISTS pt_hash_idx ON "preferred_terms" ("phonetic_hash");
CREATE INDEX IF NOT EXISTS pt_hash_type_idx ON "preferred_terms" ("phonetic_hash_type");
CREATE INDEX IF NOT EXISTS pt_term_idx ON "preferred_terms" ("term");





CREATE TABLE "lowest_level_terms" (
	"code"						INTEGER PRIMARY KEY NOT NULL,
	"term"						VARCHAR(1024) NOT NULL,
	"current"					BOOLEAN NOT NULL,
	"code__preferred_term"		INTEGER NOT NULL,
	
	"phonetic_hash"				VARCHAR(64) NOT NULL,
	"phonetic_hash_type"		VARCHAR(128) NOT NULL,
	
	FOREIGN KEY ("code__preferred_term") REFERENCES "preferred_terms" ("code")
);

CREATE INDEX IF NOT EXISTS llt_pref_term_code_idx ON "lowest_level_terms" ("code__preferred_term");
CREATE INDEX IF NOT EXISTS llt_hash_idx ON "lowest_level_terms" ("phonetic_hash");
CREATE INDEX IF NOT EXISTS llt_hash_type_idx ON "lowest_level_terms" ("phonetic_hash_type");
CREATE INDEX IF NOT EXISTS llt_term_idx ON "lowest_level_terms" ("term");
