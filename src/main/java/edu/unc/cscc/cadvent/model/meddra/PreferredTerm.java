/*-
 * ========================LICENSE_START=================================
 * cadvent
 * %%
 * Copyright (C) 2016 - 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.cadvent.model.meddra;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import edu.unc.cscc.cadvent.model.CADVEntity;

/**
 * Preferred term (see MedDRA table 1_pref_term).
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@JsonTypeName("preferred-term")
public final class PreferredTerm
implements SearchableTerm, CADVEntity
{
	
	private final int							code;
	private final String						name;
	
	private final PhoneticHash					hash;
	
	@JsonCreator
	public PreferredTerm(@JsonProperty("code") int code,
							@JsonProperty("name") String name)
	{
		this(code, name, null);
	}
	
	public PreferredTerm(int code, String name, PhoneticHash phoneticHash)
	{
		if (name == null)
		{
			throw new IllegalArgumentException("missing name");
		}
		else if (name.length() > 100)
		{
			throw new IllegalArgumentException("name must be <= 100 chars");
		}
		
		this.code = code;
		this.name = name;
		this.hash = phoneticHash;
	}
	
	/**
	 * Name of the preferred term. See MedDRA: 1_pref_term.pt_name
	 * 
	 * @return preferred term name
	 */
	@Override
	@JsonGetter("name")
	public String
	term()
	{
		return this.name;
	}
	
	/**
	 * MedDRA code of the preferred term. See MedDRA: 1_pref_term.pt_code
	 * 
	 * @return preferred term MedDRA code
	 */
	@JsonGetter("code")
	public int
	code()
	{
		return this.code;
	}

	@JsonIgnore
	@Override
	public PhoneticHash
	phoneticHash()
	{
		return this.hash;
	}
	
	@Override
	public String
	toString()
	{
		return "(pt " + this.phoneticHash() + " " + this.code() + " " + this.term() + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + code;
		result = prime * result + ((hash == null) ? 0 : hash.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PreferredTerm other = (PreferredTerm) obj;
		if (code != other.code)
			return false;
		if (hash == null)
		{
			if (other.hash != null)
				return false;
		}
		else if (!hash.equals(other.hash))
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		}
		else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	

}
