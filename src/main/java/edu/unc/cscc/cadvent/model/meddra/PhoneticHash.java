/*-
 * ========================LICENSE_START=================================
 * cadvent
 * %%
 * Copyright (C) 2016 - 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.cadvent.model.meddra;

/**
 * A phonetic hash, i.e. a sequence of characters/numbers which represents
 * the phonemes in a given string.  Many hashing strategies exist; any supported
 * by cadvent will be modeled as instances of this class.  (Supported hashing
 * strategies are themselves enumerated in the inner {@link PhoneticHashType}
 * enumeration.)
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public final class PhoneticHash
{
	
	private final PhoneticHashType	type;
	private final String			hash;
	
	/**
	 * Construct a new hash of the given type with the given hash string.
	 * 
	 * @param type hash type, not <code>null</code>
	 * @param hash hash, not <code>null</code>, otherwise unbounded
	 */
	public PhoneticHash(PhoneticHashType type, String hash)
	{
		if (type == null)
		{
			throw new IllegalArgumentException("missing type");
		}
		
		if (hash == null)
		{
			throw new IllegalArgumentException("missing hash");
		}
		
		this.type = type;
		this.hash = hash;
	}
	
	/**
	 * Get the type of the hash.
	 * 
	 * @return hash type, not <code>null</code>
	 */
	public PhoneticHashType
	type()
	{
		return this.type;
	}
	
	/**
	 * Get the hash contents.
	 * 
	 * @return hash contents, not <code>null</code>
	 */
	public String
	hash()
	{
		return this.hash;
	}
	
	/**
	 * Enumeration of the supported hash types.
	 * 
	 * @author Rob Tomsick (rtomsick@unc.edu)
	 *
	 */
	public static enum PhoneticHashType
	{
		/**
		 * Metaphone version 3
		 */
		METAPHONE_3;
	}

	public String
	toString()
	{
		return "(" + this.type().name() + " " + "\"" + this.hash() + "\"" + ")"; 
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hash == null) ? 0 : hash.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhoneticHash other = (PhoneticHash) obj;
		if (hash == null)
		{
			if (other.hash != null)
				return false;
		}
		else if (!hash.equals(other.hash))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
	
	
}
