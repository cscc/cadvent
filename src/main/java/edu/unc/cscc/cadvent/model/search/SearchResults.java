/*-
 * ========================LICENSE_START=================================
 * cadvent
 * %%
 * Copyright (C) 2016 - 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.cadvent.model.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonTypeName;

import edu.unc.cscc.cadvent.model.CADVEntity;
import edu.unc.cscc.cadvent.model.meddra.LowestLevelTerm;
import edu.unc.cscc.cadvent.model.meddra.PreferredTerm;

/**
 * The results of a search.  Contains lowest-level and preferred terms which 
 * were found by performing a search for a given query.  Also contains the
 * parameters of the query itself.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@JsonTypeName("search-results")
public class SearchResults
implements CADVEntity
{
	
	private final String				query;
	private final boolean				approximate;
	private final boolean				currentOnly;
	
	private final List<LLTResult>		lltResults;
	private final List<PTResult>		ptResults;
	
	public SearchResults(String query, Collection<LLTResult> lltResults, 
							Collection<PTResult> ptResults, 
							boolean approximate, boolean currentOnly)
	{
		this.query = query;
		this.lltResults = new ArrayList<>(lltResults);
		this.ptResults = new ArrayList<>(ptResults);
		
		this.lltResults.sort((a, b) -> a.weight() - b.weight());
		this.ptResults.sort((a, b) -> a.weight() - b.weight());
		
		this.approximate = approximate;
		this.currentOnly = currentOnly;
	}
	
	/**
	 * The search query for which results have been obtained.
	 * 
	 * @return search query, not <code>null</code>
	 */
	@JsonGetter("query")
	public String
	query()
	{
		return this.query;
	}
	
	/**
	 * Whether the search was "approximate", i.e. whether only exact matches
	 * to the query are contained in the result(s).
	 * 
	 * @return <code>true</code> if the search performed was approximate,
	 * <code>false</code> otherwise
	 */
	@JsonGetter("approximate")
	public boolean
	approximate()
	{
		return this.approximate;
	}
	
	/**
	 * Whether the search was limited to only current terms (as defined by
	 * the underlying version of the MedDRA database.)
	 * 
	 * @return <code>true</code> if the results contain only "current" terms,
	 * <code>false</code> otherwise
	 */
	@JsonGetter("current-only")
	public boolean
	currentOnly()
	{
		return this.currentOnly;
	}
	
	/**
	 * The {@link LowestLevelTerm lowest-level terms} found by this search, 
	 * in order of descending {@link SearchResult#weight() weight}.
	 * 
	 * @return lowest-level terms, not <code>null</code>
	 */
	@JsonGetter("lowest-level-terms")
	public List<LLTResult>
	lltResults()
	{
		return this.lltResults;
	}
	
	/**
	 * The {@link PreferredTerm preferred terms} found by this search, 
	 * in order of descending {@link SearchResult#weight() weight}.
	 * 
	 * @return preferred terms, not <code>null</code>
	 */
	@JsonGetter("preferred-terms")
	public final List<PTResult>
	ptResults()
	{
		return this.ptResults;
	}

	@JsonGetter(CADVEntity.API_VERSION_PROPERTY_NAME)
	@Override
	public String
	apiVersion() 
	{
		return CADVEntity.super.apiVersion();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (approximate ? 1231 : 1237);
		result = prime * result + (currentOnly ? 1231 : 1237);
		result = prime * result
				+ ((lltResults == null) ? 0 : lltResults.hashCode());
		result = prime * result
				+ ((ptResults == null) ? 0 : ptResults.hashCode());
		result = prime * result + ((query == null) ? 0 : query.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchResults other = (SearchResults) obj;
		if (approximate != other.approximate)
			return false;
		if (currentOnly != other.currentOnly)
			return false;
		if (lltResults == null)
		{
			if (other.lltResults != null)
				return false;
		}
		else if (!lltResults.equals(other.lltResults))
			return false;
		if (ptResults == null)
		{
			if (other.ptResults != null)
				return false;
		}
		else if (!ptResults.equals(other.ptResults))
			return false;
		if (query == null)
		{
			if (other.query != null)
				return false;
		}
		else if (!query.equals(other.query))
			return false;
		return true;
	}
	
	
}
