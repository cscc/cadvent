/*-
 * ========================LICENSE_START=================================
 * cadvent
 * %%
 * Copyright (C) 2016 - 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.cadvent.model.search;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import edu.unc.cscc.cadvent.model.meddra.SearchableTerm;

/**
 * A result of a search.  Specifies a weight (scale determined by search process)
 * which may be used to assess confidence in the result.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 * @param <T> type of term contained in the result
 */
@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type")
public abstract class SearchResult<T extends SearchableTerm>
{
	
	private final int		weight;
	
	SearchResult(final int weight)
	{
		this.weight = weight;
	}
	
	/**
	 * The weight of the search result.  A positive integer representing the
	 * confidence in the result, with increasing values indicating increased
	 * confidence.
	 * 
	 * @return result weight, &gte; 0
	 */
	@JsonGetter("weight")
	public int
	weight()
	{
		return this.weight;
	}
	
	/**
	 * Term found during the search.
	 * 
	 * @return term, not <code>null</code>
	 */
	@JsonGetter("term")
	public abstract T term();

	@JsonGetter("display-name")
	public String
	displayName()
	{
		return this.term().term();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + weight;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchResult<?> other = (SearchResult<?>) obj;
		if (weight != other.weight)
			return false;
		return true;
	}
	
	
	
}
