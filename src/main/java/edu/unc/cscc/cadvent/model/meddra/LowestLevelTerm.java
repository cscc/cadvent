/*-
 * ========================LICENSE_START=================================
 * cadvent
 * %%
 * Copyright (C) 2016 - 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.cadvent.model.meddra;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.annotation.JsonTypeName;

import edu.unc.cscc.cadvent.model.CADVEntity;

/**
 * Lowest-level term (see MedDRA documentation for more information.)
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@JsonTypeName("lowest-level-term")
public final class LowestLevelTerm
implements SearchableTerm, CADVEntity
{

	private final int							code;
	private final String						name;
	
	
	private final PreferredTerm					preferredTerm;
	
	private final boolean						current;
	
	private final PhoneticHash					hash;

	@JsonCreator
	public LowestLevelTerm(@JsonProperty("code") int code, 
							@JsonProperty("name") String name, 
							@JsonProperty("current") boolean current,
							@JsonProperty("preferred-term") 
								PreferredTerm preferredTerm)
	{
		this(code, name, current, preferredTerm, null);
	}
	
	public LowestLevelTerm(int code, String name, boolean current,
							PreferredTerm preferredTerm,
							PhoneticHash phoneticHash)
	{
		/* validation */
		
		if (name == null)
		{
			throw new IllegalArgumentException("missing name");
		}
		else if (name.length() > 100)
		{
			throw new IllegalArgumentException("name must be <= 100 chars");
		}
		
		if (preferredTerm == null)
		{
			throw new IllegalArgumentException("missing preferred term");
		}
		
		this.code = code;
		this.name = name;
		this.current = current;
		this.preferredTerm = preferredTerm;
		
		this.hash = phoneticHash;
	}
	
	/**
	 * Name of the lowest-level term.  See MedDRA: 1_low_level_term.llt_name
	 * 
	 * @return lowest-level term name, not <code>null</code>
	 */
	@Override
	@JsonGetter("name")
	public final String
	term()
	{
		return this.name;
	}
	
	/**
	 * MedDRA code of the lowest-level term.  
	 * See MedDRA: 1_low_level_term.llt_code
	 * 
	 * @return lowest-level term code
	 */
	@JsonGetter("code")
	public final int
	code()
	{
		return this.code;
	}
	
	/**
	 * Preferred term with which the lowest-level term is associated.
	 * 
	 * @return preferred term, not <code>null</code>
	 */
	@JsonGetter("preferred-term")
	@JsonTypeInfo(use = Id.NONE)
	public final PreferredTerm
	preferredTerm()
	{
		return this.preferredTerm;
	}
	
	/**
	 * Whether the lowest-level term is considered current.
	 * 
	 * @return <code>true</code> if the term is current, <code>false</code>
	 * otherwise
	 */
	@JsonGetter("current")
	public final boolean
	current()
	{
		return this.current;
	}
	
	@Override
	public PhoneticHash 
	phoneticHash()
	{
		return this.hash;
	}

	@Override
	public String
	toString()
	{
		return "(llt " + this.phoneticHash() + " "  + this.code() + " " + this.term() + " " 
					+ this.preferredTerm().toString() + " " + (this.current() ? "#t" : "#f") + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + code;
		result = prime * result + (current ? 1231 : 1237);
		result = prime * result + ((hash == null) ? 0 : hash.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((preferredTerm == null) ? 0 : preferredTerm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LowestLevelTerm other = (LowestLevelTerm) obj;
		if (code != other.code)
			return false;
		if (current != other.current)
			return false;
		if (hash == null)
		{
			if (other.hash != null)
				return false;
		}
		else if (!hash.equals(other.hash))
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		}
		else if (!name.equals(other.name))
			return false;
		if (preferredTerm == null)
		{
			if (other.preferredTerm != null)
				return false;
		}
		else if (!preferredTerm.equals(other.preferredTerm))
			return false;
		return true;
	}
	
	
	
}
