/*-
 * ========================LICENSE_START=================================
 * cadvent
 * %%
 * Copyright (C) 2016 - 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.cadvent.db;

import java.util.Collection;
import java.util.Map;

import edu.unc.cscc.cadvent.model.meddra.PhoneticHash;
import edu.unc.cscc.cadvent.model.meddra.SearchableTerm;
import edu.unc.cscc.cadvent.model.search.SearchResult;

public interface SearchableTermStorage<T extends SearchableTerm>
{

	/**
	 * Find any terms which match the given query or have similarity to the 
	 * given phonetic hash.  If the phonetic hash type is not supported by
	 * underlying storage, this search will function based on the query string
	 * alone.
	 * 
	 * @param query search string, not <code>null</code>, may be empty
	 * @param hash phonetic hash of the query string to use when searching
	 * the underlying storage, not <code>null</code>
	 * 
	 * @return matching terms, not <code>null</code>, may be empty
	 */
	public Collection<? extends SearchResult<T>> search(String query, PhoneticHash hash);
	
	/**
	 * Load all of the searchable terms with the given code(s).  
	 * 
	 * @param codes codes of terms to load, not <code>null</code>
	 * 
	 * @return map of codes to terms, not <code>null</code>
	 */
	public Map<Integer, T> loadAll(Collection<Integer> codes);
	
	/**
	 * Save the given term to underlying storage.
	 * 
	 * @param term term to save, not <code>null</code>
	 * 
	 * @return copy of the saved term, not <code>null</code>
	 */
	public T save(T term);
	
	/**
	 * Save all of the given terms to the underlying storage.  Functionally 
	 * equivalent to <code>terms.forEach(t -> this.save(t))</code>.
	 * 
	 * @param terms terms to save, not <code>null</code>
	 */
	public void saveAll(Collection<T> terms);
	
	
}
