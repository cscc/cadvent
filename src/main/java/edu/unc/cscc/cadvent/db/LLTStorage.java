/*-
 * ========================LICENSE_START=================================
 * cadvent
 * %%
 * Copyright (C) 2016 - 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.cadvent.db;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.lower;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.table;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.jooq.BatchBindStep;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.unc.cscc.cadvent.model.meddra.LowestLevelTerm;
import edu.unc.cscc.cadvent.model.meddra.PhoneticHash;
import edu.unc.cscc.cadvent.model.meddra.PhoneticHash.PhoneticHashType;
import edu.unc.cscc.cadvent.model.meddra.PreferredTerm;
import edu.unc.cscc.cadvent.model.search.LLTResult;

@Service
public final class LLTStorage 
implements SearchableTermStorage<LowestLevelTerm>
{
	
	private static final Table<?>		TABLE = table(name("lowest_level_terms"));
	
	private final DSLContext			ctx;
	private final PTStorage				ptStorage;
	
	
	@Autowired
	public LLTStorage(DSLContext ctx, PTStorage ptStorage)
	{
		this.ctx = ctx;
		this.ptStorage = ptStorage;
	}
	

	@Override
	public Collection<LLTResult> 
	search(String query, PhoneticHash hash)
	{		
		
		Map<LowestLevelTerm, Integer> scores = new HashMap<>();
		
		
		
		final Set<Integer> codes = new HashSet<>();
		
		final Set<Integer> exact = 
				new HashSet<>(
						this.ctx
							.select(field(name("code"), Integer.class))
							.from(TABLE)
							.where(lower(field(name("term"), String.class))
										.eq(query.toLowerCase()))
							.fetch(field(name("code"), Integer.class)));
		
		if (! exact.isEmpty())
		{
			final Map<Integer, LowestLevelTerm> terms = 
					this.loadAll(exact);
			
			terms.values().forEach(t -> scores.put(t, 100));
		}
		
		/* because we're doing a two-part load to get our PTs, we do all of 
		 * our searches up front and save scoring for later
		 */
		
		
		codes.addAll(this.ctx
						.select(field(name("code"), Integer.class))
						.from(TABLE)
						.where(lower(field(name("term"), String.class))
								.contains(query.toLowerCase()))
						.fetch(field(field(name("code"), Integer.class))));
		
		codes.addAll(this.ctx
						.select(field(name("code"), Integer.class))
						.from(TABLE)
						.where(field(name("phonetic_hash")).contains(hash.hash())
								.and(field(name("phonetic_hash_type")).eq(hash.type().name())))
						.fetch(field(name("code"), Integer.class)));
		
		Map<Integer, LowestLevelTerm> fuzzyMatches = this.loadAll(codes);
		
		/* add all our fuzzy matches to our score set */
		fuzzyMatches.forEach((_c, match) -> scores.put(match, 0));
		
		/* scoring! */
		
		final int MAX_SCORE = 90;		
		
		scores.replaceAll((term, _s) -> {
			
			/* early exit for perfect matches */
			if (_s >= MAX_SCORE)
			{
				return _s;
			}
			
			
			
			final int TEXT_SCORE = 40;
			final int HASH_SCORE = MAX_SCORE - TEXT_SCORE;
			
			
			
			double bias = 0; 
			
			/* bias: if query is start of candidate term, award 30% bonus */
			bias += (term.term().toLowerCase().startsWith(query.toLowerCase()))
						? TEXT_SCORE * 0.3d
						: 0;
			
			bias += (term.term().toLowerCase().endsWith(query.toLowerCase()))
					? TEXT_SCORE * 0.3d
					: 0;
			
			/* bias if query is stand-alone word in candidate term or query is ending of term */
			bias += (term.term().toLowerCase().endsWith(query.toLowerCase())
					|| term.term().toLowerCase().matches("(.*\\s+\\Q" + query.toLowerCase() + "\\E\\s+.*)"))
						? TEXT_SCORE * 0.2d
						: 0;
			
			
			double ts = 
					((double) TEXT_SCORE) * StringUtils.getJaroWinklerDistance(query, term.term());
			
			/* now compute the hash score */
			
			
			double hs = 0.0d;
			
			if (term.phoneticHash() != null)
			{
				hs = HASH_SCORE * ((double) hash.hash().length() / (double) term.phoneticHash().hash().length());
				
				bias += (term.phoneticHash().hash().startsWith(hash.hash()))
						? HASH_SCORE * 0.3d
						: 0;
			}
			
			
			final int total = (int) Math.round(ts + hs + bias);
			
			return total;
		});
		
		/* finally, normalize all scores from 0 - 90 */
		
		float max = scores.values().stream().reduce((a, b) -> a > b ? a : b).orElse(0);
		float min = scores.values().stream().reduce((a, b) -> a < b ? a : b).orElse(0);
		
		scores.replaceAll((_1, score) -> Math.round(((float) score - min) / (max - min) * ((float) MAX_SCORE)));
		
		return scores.entrySet()
				.stream()
				.map(e -> new LLTResult(e.getKey(), e.getValue()))
				.collect(Collectors.toSet());
		
	}
	
	@Override
	public Map<Integer, LowestLevelTerm> 
	loadAll(Collection<Integer> codes)
	{
		if (codes.isEmpty())
		{
			return Collections.emptyMap();
		}
		
		Collection<Integer> ptCodes = 
				this.ctx
					.select(field(name("code__preferred_term")))
					.from(TABLE)
					.where(field(name("code")).in(codes))
					.fetch(field(name("code__preferred_term"), Integer.class));
		
		Map<Integer, PreferredTerm> pts = 
				this.ptStorage.loadAll(ptCodes);
		
		Map<Integer, LowestLevelTerm> returnMap = new HashMap<Integer, LowestLevelTerm>();
		Result<?> result = this.ctx
				.selectFrom(TABLE)
				.where(field(name("code")).in(new HashSet<>(codes)))
				.fetch();
		for (Record r : result) {
			returnMap.put(r.get(field(name("code"), Integer.class)),
					new LowestLevelTerm(
							r.get(field(name("code")), Integer.class), 
							r.get(field(name("term")), String.class), 
							r.get(field(name("current")), Boolean.class), 
							pts.get(r.get(field(name("code__preferred_term")), 
										Integer.class)),
							new PhoneticHash(
									PhoneticHashType.valueOf(r.get(field(name("phonetic_hash_type")), String.class)), 
									r.get(field(name("phonetic_hash")), String.class))));
		}
		return returnMap;
		
//		return this.ctx
//					.selectFrom(TABLE)
//					.where(field(name("code")).in(new HashSet<>(codes)))
//					.fetchMap(field(name("code"), Integer.class),
//							r -> new LowestLevelTerm(
//									r.get(field(name("code")), Integer.class), 
//									r.get(field(name("term")), String.class), 
//									r.get(field(name("current")), Boolean.class), 
//									pts.get(r.get(field(name("code__preferred_term")), 
//												Integer.class)),
//									new PhoneticHash(
//											PhoneticHashType.valueOf(r.get(field(name("phonetic_hash_type")), String.class)), 
//											r.get(field(name("phonetic_hash")), String.class))));
	}


	@Override
	public LowestLevelTerm 
	save(LowestLevelTerm term)
	{
		
		this.ctx.insertInto(TABLE)
				.set(field(name("code")), term.code())
				.set(field(name("term")), term.term())
				.set(field(name("code__preferred_term")), term.preferredTerm().code())
				.set(field(name("current")), term.current())
				.set(field(name("phonetic_hash_type")), term.phoneticHash().type().name())
				.set(field(name("phonetic_hash")), term.phoneticHash().hash())
				.execute();
		
		return term;
	}
	
	private final BatchBindStep
	createInsertBindStep()
	{
		return this.ctx.batch(this.ctx
						.insertInto(TABLE)
						.columns(field(name("code"), Integer.class),
								field(name("term"), String.class),
								field(name("code__preferred_term"), Integer.class),
								field(name("current"), Boolean.class),
								field(name("phonetic_hash"), String.class),
								field(name("phonetic_hash_type"), String.class))
						.values(0, null, 0, false, null, null));
	}
	
	@Override
	public void 
	saveAll(Collection<LowestLevelTerm> terms)
	{
		final int BATCH_SIZE = 50000;
		
		BatchBindStep batch = this.createInsertBindStep();
				
							
		int i = BATCH_SIZE;
		for (final LowestLevelTerm term : terms)
		{
			batch.bind(term.code(), term.term(), 
						term.preferredTerm().code(),
						term.current(),
						term.phoneticHash().hash(),
						term.phoneticHash().type().name());
			
			if (--i < 1)
			{
				batch.execute();
				batch = this.createInsertBindStep();
				i = BATCH_SIZE;
			}
		}
		
		if (i != BATCH_SIZE)
		{
			batch.execute();
		}
		
	}

}
