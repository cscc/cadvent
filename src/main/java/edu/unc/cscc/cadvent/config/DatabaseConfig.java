/*-
 * ========================LICENSE_START=================================
 * cadvent
 * %%
 * Copyright (C) 2016 - 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.cadvent.config;

import java.io.IOException;
import java.sql.Driver;

import javax.sql.DataSource;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultDSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.ConnectionProperties;
import org.springframework.jdbc.datasource.embedded.DataSourceFactory;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DatabaseConfig
{
	@Bean
	public DataSource
	datasource()
	throws IOException
	{
		
		final HikariDataSource ds = new HikariDataSource();
		
		ds.setMaximumPoolSize(4);
		ds.setIdleTimeout(0);
		ds.setMaxLifetime(0);
		
		 return new EmbeddedDatabaseBuilder()
				 	.setDataSourceFactory(new DataSourceFactory()
					{	
						@Override
						public DataSource
						getDataSource()
						{
							return ds;
						}
						
						@Override
						public ConnectionProperties
						getConnectionProperties()
						{
							return new ConnectionProperties()
							{
								
								@Override
								public void setUsername(String username)
								{
									ds.setUsername(username);
								}
								
								@Override
								public void setUrl(String url)
								{
									ds.setJdbcUrl(url);
								}
								
								@Override
								public void setPassword(String password)
								{
									ds.setPassword(password);
								}
								
								@Override
								public void setDriverClass(Class<? extends Driver> driverClass)
								{
									ds.setDriverClassName(driverClass.getCanonicalName());
								}
							};
						}
					})
		            .generateUniqueName(true)
		            .setType(EmbeddedDatabaseType.HSQL)
		            .setScriptEncoding("UTF-8")
		            .ignoreFailedDrops(true)
		            .build();		
	}
		
	@Bean
	@Autowired
	public DSLContext
	ctx(DataSource datasource)
	{
		return new DefaultDSLContext(datasource, SQLDialect.HSQLDB);
	}
	
}
