/*-
 * ========================LICENSE_START=================================
 * cadvent
 * %%
 * Copyright (C) 2016 - 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.cadvent.config;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import edu.unc.cscc.cadvent.db.LLTStorage;
import edu.unc.cscc.cadvent.db.PTStorage;
import edu.unc.cscc.cadvent.loader.LLTLoader;
import edu.unc.cscc.cadvent.loader.MapBackedTermLookup;
import edu.unc.cscc.cadvent.loader.PTLoader;
import edu.unc.cscc.cadvent.model.meddra.LowestLevelTerm;
import edu.unc.cscc.cadvent.model.meddra.PreferredTerm;

/**
 * Responsible for loading the required MedDRA datasets into the backing 
 * database.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@Configuration
public class MedDRAConfig
{
	
	private final String			baseDirectory;
	
	private final String			lltFilename;
	private final String			ptFilename;
	
	
	public MedDRAConfig(@Value("${cadvent.base_directory}") String baseDirectory,
						@Value("${cadvent.llt_filename}") String ltFilename,
						@Value("${cadvent.pt_filename}") String ptFilename)
	{
		this.baseDirectory = baseDirectory;
		this.lltFilename = ltFilename;
		this.ptFilename = ptFilename;
	}
	
	
	/**
	 * Initialize the given {@link PreferredTerm preferred term} and 
	 * {@link LowestLevelTerm lowest-level term} storage with MedDRA data
	 * from the configured dataset sources. 
	 * 
	 * @param ptStorage preferred term storage
	 * @param ltStorage lowest-level term storage
	 * @throws IOException
	 */
	@Autowired
	public final void
	initializeDB(PTStorage ptStorage, LLTStorage ltStorage)
	throws IOException
	{
		final Logger logger = LoggerFactory.getLogger(this.getClass());
		
		final File llt = new File(this.baseDirectory, this.lltFilename);
		
		final File ptFile = new File(this.baseDirectory, this.ptFilename);
		
		if (! llt.canRead() || ! ptFile.canRead())
		{
			throw new IOException("Cannot read LLT or PT dictionary files.  "
									+ "Check that they exist at the paths you "
									+ "have specified.");
		}
		
		InputStream pts = 
				new BufferedInputStream(new FileInputStream(ptFile));
		
		InputStream llts = 
				new BufferedInputStream(new FileInputStream(llt));
		
		
		logger.info("Loading PT -> DB");
		
		PTLoader ptl = new PTLoader(pts);
				
		Map<Integer, PreferredTerm> preferredTerms = new HashMap<>();
		PreferredTerm pt;
		while ((pt = ptl.loadTerm()) != null)
		{
			preferredTerms.put(pt.code(), pt);
		}
		
		ptStorage.saveAll(preferredTerms.values());
		
		logger.info("Loaded " + preferredTerms.values().size() + " PTs");
			
		
		logger.info("Loading LLT -> DB");
		LLTLoader lltLoader = new LLTLoader(llts, new MapBackedTermLookup<>(preferredTerms));
		
		LowestLevelTerm lt;
		List<LowestLevelTerm> lowestLevelTerms = new LinkedList<>();
		int count = 0;
		
		while ((lt = lltLoader.loadTerm()) != null)
		{
			lowestLevelTerms.add(lt);
			count++;
		}
		
		ltStorage.saveAll(lowestLevelTerms);
		
		logger.info("Loaded " + count + " LLTs");
		
		logger.info("Done loading MedDRA dictionaries");
	}

}
