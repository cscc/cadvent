/*-
 * ========================LICENSE_START=================================
 * cadvent
 * %%
 * Copyright (C) 2016 - 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.cadvent.third_party;

import edu.unc.cscc.cadvent.model.meddra.PhoneticHash;
import edu.unc.cscc.cadvent.model.meddra.PhoneticHash.PhoneticHashType;

/**
 * Phonetic encoder utility functions.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public abstract class PhoneticEncoder
{
	
	/**
	 * Produce a phonetic hash containing the Metaphone 3 encoding of the 
	 * given string.  The given encoding may be up to the maximum length 
	 * supported by the official Metaphone 3 length, and will include only 
	 * the primary encoding.
	 * 
	 * @param str string to encode, not <code>null</code>
	 * @return hash containing a Metaphone 3 encoding of the given string,
	 * not <code>null</code>
	 */
	public static final PhoneticHash
	encodeMetaphone3(String str)
	{
		if (str == null)
		{
			throw new IllegalArgumentException("str must not be null");
		}
		
		final Metaphone3 m = new Metaphone3(str);
		
		m.SetKeyLength(m.GetMaximumKeyLength());
		
		m.Encode();
		
		return new PhoneticHash(PhoneticHashType.METAPHONE_3, m.GetMetaph());
	}

}
