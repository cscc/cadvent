/*-
 * ========================LICENSE_START=================================
 * cadvent
 * %%
 * Copyright (C) 2016 - 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.cadvent.loader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

import org.springframework.util.StringUtils;

import edu.unc.cscc.cadvent.model.meddra.SearchableTerm;

/**
 * Term loader; base class for implementations which can load a 
 * {@link SearchableTerm searchable term} from an delimited input stream
 * (ala MedDRA ASCII).  Will require modification if used for non-MedDRA 
 * formats.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 * @param <Term> type of term produced by loader
 */
abstract class TermLoader<Term extends SearchableTerm>
{

	private InputStream		stream;
	private BufferedReader	reader;
	
	private final int				numFields;
	
	protected TermLoader(InputStream stream, int numFields)
	{
		this.stream = stream;
		this.reader = new BufferedReader(new InputStreamReader(stream));
		this.numFields = numFields;
	}
	
	/**
	 * Load the next available term from the underlying input stream, if one
	 * is available.
	 * 
	 * @return next term, or <code>null</code> if the underlying stream has 
	 * been exhausted
	 * @throws IOException thrown if the underlying stream could not be read
	 * or parsed
	 */
	public Term 
	loadTerm()
	throws IOException
	{
		if (this.reader == null)
		{
			throw new IOException("Cannot load term from closed loader");
		}
		
		final String line = this.reader.readLine();
		
		if (line == null)
		{
			return null;
		}
		
		String[] fields = StringUtils.delimitedListToStringArray(line, "$");
		
		fields = Arrays.copyOfRange(fields, 0, numFields);
		
		if (fields.length != numFields)
		{
			throw new IOException(
					"Incorrect number of fields parsed from line. Expected " 
							+ numFields + ", got " + fields.length);
		}
		
		return this.createTerm(fields);
		
	}
	
	
	/**
	 * Close the reader and underlying {@link InputStream} used by this loader.
	 * 
	 * @throws IOException
	 */
	public void
	close()
	throws IOException
	{
		this.reader.close();
		this.stream.close();
		
		this.reader = null;
		this.stream = null;
	}

	/**
	 * Create a term of the type produced by the loader from the given fields.
	 * 
	 * @param fields fields from which to create term, not <code>null</code>
	 * @return term created from the given fields, not <code>null</code>
	 * @throws TermParseException thrown if the term could not be created from
	 * the given fields
	 */
	protected abstract Term
	createTerm(String[] fields)
	throws TermParseException;

	
	
	
}
