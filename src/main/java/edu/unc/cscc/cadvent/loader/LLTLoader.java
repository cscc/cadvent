/*-
 * ========================LICENSE_START=================================
 * cadvent
 * %%
 * Copyright (C) 2016 - 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.cadvent.loader;

import java.io.InputStream;

import edu.unc.cscc.cadvent.model.meddra.LowestLevelTerm;
import edu.unc.cscc.cadvent.model.meddra.PreferredTerm;
import edu.unc.cscc.cadvent.third_party.PhoneticEncoder;

/**
 * Loader for {@link LowestLevelTerm lowest-level terms}.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public final class LLTLoader
extends TermLoader<LowestLevelTerm>
{
	
	private final TermLookup<Integer, PreferredTerm>	pts;

	public LLTLoader(InputStream stream, 
					TermLookup<Integer, PreferredTerm> preferredTermLookup)
	{
		super(stream, 11);
		this.pts = preferredTermLookup;
	}

	@Override
	protected LowestLevelTerm 
	createTerm(String[] fields)
	throws TermParseException
	{
		Integer code;
		Integer ptCode;
		try
		{
			code = Integer.valueOf(fields[0]);
			
			ptCode = Integer.valueOf(fields[2]);
		}
		catch (NumberFormatException e)
		{
			throw new TermParseException("Unable to parse code as long", e);
		}
		
		final boolean current = "Y".equals(fields[9]);
				
		final PreferredTerm preferredTerm = this.pts.lookup(ptCode);
		
		if (preferredTerm == null)
		{
			throw new TermParseException(
					"Unknown preferred term (code " + ptCode + ")");
		}
		
		final String term = fields[1];
		
		
		return new LowestLevelTerm(code, term, current, preferredTerm, 
									PhoneticEncoder.encodeMetaphone3(term));
	}

}
