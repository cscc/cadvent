/*-
 * ========================LICENSE_START=================================
 * cadvent
 * %%
 * Copyright (C) 2016 - 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.cadvent.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.unc.cscc.cadvent.db.LLTStorage;
import edu.unc.cscc.cadvent.db.PTStorage;
import edu.unc.cscc.cadvent.model.meddra.PhoneticHash;
import edu.unc.cscc.cadvent.model.meddra.SearchableTerm;
import edu.unc.cscc.cadvent.model.search.LLTResult;
import edu.unc.cscc.cadvent.model.search.PTResult;
import edu.unc.cscc.cadvent.third_party.PhoneticEncoder;

@RestController
public class SearchController
{

	@Autowired
	private LLTStorage			lltStorage;
	@Autowired
	private PTStorage			ptStorage;
	
	
	/**
	 * Search the underlying storage for terms matching the given query.  Will
	 * perform both phonetic and text searches against the configured 
	 * {@link SearchableTerm searchable term} storage.
	 * 
	 * @param query query for which to load terms
	 * @param includeLLT <code>true</code> if lowest-level terms should be
	 * included in the search, <code>false</code> otherwise
	 * @param includePT <code>true</code> if preferred terms should be included
	 * in the search, <code>false</code> otherwise
	 * @param includeObsolete <code>true</code> if non-current terms should
	 * be included in the results, <code>false</code> otherwise
	 * @param limit maximum number of lowest level and maximum number of 
	 * preferred terms which should be returned, must be a positive integer 
	 * 	between 1 and 1000
	 * 
	 * @return search results
	 */
	@RequestMapping(path = "/search",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE
					)
	@Validated
	public ResponseEntity<?>
	search(@RequestParam(value = "query", required = true)
				@Valid @NotNull String query,
			@RequestParam(value = "llt", required = false,
							defaultValue = "true")
			boolean includeLLT,
			@RequestParam(value = "pt", required = false, 
							defaultValue = "true")
			boolean includePT,
			@RequestParam(value = "include-obsolete", defaultValue = "false", 
							required = false)
			boolean includeObsolete,
			@RequestParam(value = "limit", required = false, 
							defaultValue = "30")
			@Valid @Min(1) @Max(1000)
			int limit)
	
	{
		
		if (query == null || query.isEmpty())
		{
			return new ResponseEntity<>("missing query", HttpStatus.BAD_REQUEST);
		}
		
		if (! includeLLT && ! includePT)
		{
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
		final PhoneticHash hash = PhoneticEncoder.encodeMetaphone3(query);
		
		List<LLTResult> llts = new ArrayList<>();
		
		if (includeLLT)
		{
			llts.addAll(this.lltStorage.search(query, hash));
		}
		
		final List<PTResult> pts = new ArrayList<>();
		if (includePT)
		{
			pts.addAll(this.ptStorage.search(query, hash));
		}
		
		/* filter current */
		if (includeLLT && ! includeObsolete)
		{
			llts = llts.stream()
						.filter(lltr -> lltr.term().current())
						.collect(Collectors.toList());
		}
		
		
		/* TODO - move some of the scoring down into the service layer */
		
		List<PTResult> pRes = new ArrayList<>(pts);
		
		pRes.sort((a, b) -> b.weight() - a.weight());
		
		final int pLimit = pRes.size() > limit ? limit : pRes.size();
		pRes = pRes.subList(0, pLimit);
		
		List<LLTResult> lRes = new ArrayList<>(llts);
		lRes.sort((a, b) -> b.weight() - a.weight());
		
		final int lLimit = lRes.size() > limit ? limit : lRes.size();
		
		lRes = lRes.subList(0, lLimit);
		
		List<Result<?>> results = new ArrayList<>();
		
		results.addAll(lRes
				.stream()
				.map(p -> Result.of(p, p.displayName()))
				.collect(Collectors.toList()));
		
		results.addAll(pRes
				.stream()
				.map(p -> Result.of(p, p.displayName()))
				.collect(Collectors.toList()));		
		
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

}
