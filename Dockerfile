# This file is a template, and might need editing before it works on your project.
FROM maven:alpine

COPY . /usr/src/cadvent
WORKDIR /usr/src/cadvent

RUN mvn clean package

FROM openjdk:8u131

COPY --from=0 /usr/src/cadvent/target/cadvent.jar /opt/app.jar
COPY --from=0 /usr/src/cadvent/datafiles/* /data/

CMD java $JAVA_OPTIONS -jar /opt/app.jar --cadvent.base_directory=/data/ --server.port=8084

EXPOSE 8084
